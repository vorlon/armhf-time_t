dir=reports

all: $(dir)/runtime-libs $(dir)/source-packages
all: $(dir)/reverse-depends-sources $(dir)/maintainer-list
all: $(dir)/lfs-and-depends-time_t $(dir)/lfs-source-packages
all: $(dir)/lfs-reverse-depends-sources $(dir)/ftp-override-map.txt

results/results_failed.txt results/results_uninstallable.txt \
results/results_time_t.txt:
	mkdir -p results
	cd results && \
	wget --mirror -nd -l 1 -R index.html -R index.html.bak \
		https://adrien.dcln.fr/misc/armhf-time_t/2024-02-26T12%3A07%3A00/summary/

$(dir)/shlibs-list: packagelist.py
	./packagelist.py > $@.new
	mv $@.new $@

$(dir)/runtime-libs: results/results_failed.txt
$(dir)/runtime-libs: results/results_uninstallable.txt
$(dir)/runtime-libs: results/results_time_t.txt $(dir)/shlibs-list
	set -e; \
	sed -e's/=.*//' results/results_failed.txt \
		results/results_uninstallable.txt \
		results/results_time_t.txt \
	| sort -u \
	| while read devpkg; do \
		for shlib in $$(sed -n -e"s/^$$devpkg: //p" < $(dir)/shlibs-list); \
		do \
			echo $$shlib; \
		done; \
	done | sort -u > $@.new
	-wc -l $@ $@.new
	-diff -u $@ $@.new
	mv $@.new $@

# also depends on package indices
$(dir)/source-packages: $(dir)/runtime-libs
	set -e; \
	while read pkg; do \
		for src in $$(cat packages-unstable-* \
		| grep-dctrl -FPackage -X $$pkg -o -FProvides -w $$pkg \
			-sSource:Package -n | sed -e's/ .*//'); do \
			echo "$$src: $$pkg"; \
		done; \
	done < $< | sort -u > $@.new
	set -e; prevsrc=; prevbin=; while read src bin; do \
		if [ "$$prevsrc" = "$$src" ]; then \
			prevbin="$$prevbin $$bin"; \
		else \
			[ -n "$$prevsrc" ] && echo $$prevsrc $$prevbin; \
			prevsrc=$$src; \
			prevbin=$$bin; \
		fi; \
	done < $@.new > $@; echo $$prevsrc $$prevbin >> $@
	rm -f $@.new

$(dir)/reverse-depends-sources: $(dir)/runtime-libs $(dir)/source-packages
	set -e; \
	cat $(dir)/runtime-libs | while read libpkg; do \
		grep-dctrl -FDepends,Pre-Depends -sSource:Package -n \
			-w $$libpkg \
			packages-unstable-*armhf \
		| sed -e's/ .*//'; \
	done | sort -u > $@.new
	sed -e's/:.*//' $(dir)/source-packages | sort -u | join -j1 -v2 - \
		$@.new > $@
	rm $@.new

$(dir)/maintainer-list: $(dir)/source-packages $(dir)/lfs-source-packages
	cat $^ | sed -e's/:.*//' | dd-list -i > $@.new
	mv $@.new $@

$(dir)/lfs-conflicts: results/results_lfs_not_time_t.txt $(dir)/shlibs-list
$(dir)/lfs-conflicts: $(dir)/reverse-depends-sources
	lfs_shlibs=$$( \
		sed -e's/=.*//' results/results_lfs_not_time_t.txt | sort -u \
		| while read devpkg; \
		do \
			for shlib in $$(sed -n -e"s/^$$devpkg: //p" \
					< reports/shlibs-list); \
			do \
				echo $$shlib; \
			done; \
		done | sort -u); \
	:> $@.new; \
	for shlib in $$lfs_shlibs; \
	do \
		revdeps=$$(grep-dctrl -FDepends,Pre-Depends -sSource:Package \
				-n -w $$shlib packages-unstable-*armhf \
		           | sed -e's/ .*//'; apt-cache showsrc $$shlib \
		           | sed -n -e's/Package: //p'); \
		for revdep in $$revdeps; \
		do \
			if grep -q "^$$revdep$$" reports/reverse-depends-sources; \
			then \
				echo $$revdep: $$shlib >> $@.new; \
			fi \
		done; \
	done
	mv $@.new $@

$(dir)/lfs-and-depends-time_t: $(dir)/lfs-conflicts $(dir)/runtime-libs
	sed -e's/.*: //' $< | sort -u > $@.new
	join -j1 -v2 $(dir)/runtime-libs $@.new > $@
	rm $@.new

$(dir)/lfs-source-packages: $(dir)/lfs-conflicts $(dir)/source-packages
	for pkg in $$(sed -e's/.*: //' $< | sort -u); do \
		echo $$(apt-cache showsrc $$pkg | sed -n -e's/Package: //p'|sort -u): $$pkg; \
	done | sort -u > $@.new
	(set -e; prevsrc=; prevbin=; while read src bin; do \
		if [ "$$prevsrc" = "$$src" ]; then \
			prevbin="$$prevbin $$bin"; \
		else \
			[ -n "$$prevsrc" ] && echo $$prevsrc $$prevbin; \
			prevsrc=$$src; \
			prevbin=$$bin; \
		fi; \
	done < $@.new; echo $$prevsrc $$prevbin) \
	| join -j1 -v2 $(dir)/source-packages - > $@
	rm $@.new

$(dir)/lfs-reverse-depends-sources: $(dir)/lfs-conflicts \
		$(dir)/reverse-depends-sources $(dir)/lfs-and-depends-time_t \
		$(dir)/source-packages
	for libpkg in $$(sed -e's/.*: //' $< | sort -u); do \
		grep-dctrl -FDepends,Pre-Depends -sSource:Package -n -w \
			$$libpkg packages-unstable-*armhf \
		| sed -e's/ .*//'; \
	done | sort -u > $@.new
	join -j1 -v2 $(dir)/lfs-and-depends-time_t $@.new > $@.new2
	rm $@.new
	join -j1 -v2 $(dir)/reverse-depends-sources $@.new2 > $@.new
	rm $@.new2
	sed -e's/:.*//' $(dir)/source-packages | sort -u \
	| join -j1 -v2 - $@.new > $@
	rm $@.new

$(dir)/ftp-override-map.txt: $(dir)/source-packages $(dir)/lfs-source-packages
	set -e; cat $^ | while read source bins; \
	do \
		for bin in $$bins; \
		do \
			case $$bin in \
				*t64*) \
					continue ;; \
			esac; \
			newbin=$$(echo $$bin | sed -r -e's/([0-9+])(c102|c2|c2a|[abcdeg]|ldbl|v5|gf|nc6|debian)?(-heimdal|-gnutls|-gcrypt|-nss|-openssl|-qt|-base|-extras|linux|-search|-dbg|-gles)?$$/\1t64\3/'); \
			if [ "$$bin" = libc-ares2 ]; then \
				echo libc-ares2 libcares2; \
			elif [ "$$bin" = libident ]; then \
				echo libident libident0; \
			elif [ "$$bin" = libzia ]; then \
				echo libzia libzia-4.36; \
			elif [ "$$bin" = libspread-sheet-widget ]; then \
				echo libspread-sheet-widget libspread-sheet-widget0; \
			elif [ "$$bin" = libsmbclient ]; then \
				echo libsmbclient libsmbclient0; \
			elif [ "$$bin" = libsoup2.4-1 ]; then \
				echo libsoup2.4-1 libsoup-2.4-1; \
			elif [ "$$bin" = libsoup-gnome2.4-1 ]; then \
				echo libsoup-gnome2.4-1 libsoup-gnome-2.4-1; \
			elif [ "$$bin" != "$$newbin" ]; then \
				echo $$bin $$newbin; \
			fi; \
			if [ "$$bin" = libc-client2007e ]; then \
				echo "libc-client2007e-dev libc-client2007t64-dev"; \
			fi; \
		done; \
	done | sort -u > $@.new
	mv $@.new $@

clean:
	rm -f contents-unstable-*
	rm -f packages-unstable-*
	rm -f $(dir)/*
	rm -f results/*

