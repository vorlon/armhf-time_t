#!/usr/bin/env python3
import argparse
import mailbox
import os
import sys

from reportbug import debbugs
from reportbug import urlutils


TIME_T_SUBJECT = 'NMU diff for 64-bit time_t transition'


def get_debdiff_from_msg(msg):
    if not msg.is_multipart():
        return None

    for part in msg.get_payload():
        if part.is_multipart():
            debdiff = get_debdiff_from_msg(part)
            if debdiff is not None:
                return debdiff

            continue

        filename = part.get_filename()
        if filename is not None and filename.endswith('.debdiff'):
            return part.get_payload(decode=True)

    return None


def get_latest_patch(mbox):
    for msg in reversed(mbox.values()):
        debdiff = get_debdiff_from_msg(msg)
        if debdiff is not None:
            return debdiff

    raise Exception('no debdiff found in mbox')


def get_bug_mbox(bugnum):
    tmpfile = f'/tmp/bts_{bugnum}.mbox'

    url = debbugs.get_report_url('debian', bugnum, False, mbox=True)
    report = urlutils.open_url(url, '', timeout=60)

    with open(tmpfile, 'w+') as f:
        f.write(report)

    mbox = mailbox.mbox(tmpfile)

    try:
        os.remove(tmpfile)
    except:
        pass

    return mbox


def get_time_t_bug_patch(package):
    bugs = []

    (count, _, hierarchy) = debbugs.get_reports(package, 60, source=True)

    if not count or not hierarchy:
        raise Exception(f'no bugs found for {package}')

    for entry in hierarchy:
        # Entries are tuples of the form (title, bug_list)
        #
        # Ignore bugs that don't match our subject string, and bugs that are
        # already resolved.
        bugs += [
            b for b in entry[1]
            if all((
                TIME_T_SUBJECT in b.subject,
                'patch' in b.tags,
                'pending' in b.tags,
                b.pending != 'done'
            ))
        ]

    if not bugs:
        raise Exception(f'no open 64-bit time_t bugs found for {package}')

    # Take the newest bug (i.e. highest bug number), in case multiple bugs have
    # been filed.
    bug = max([b.bug_num for b in bugs])
    mbox = get_bug_mbox(bug)

    return (bug, get_latest_patch(mbox))


def main():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(sys.argv[0]),
        description='Fetch package debdiff for 64-bit time_t transition',
    )
    parser.add_argument('package')

    args = parser.parse_args()

    try:
        (bug, patch) = get_time_t_bug_patch(args.package)

        # Indicate where we got the patch from on stderr, and write the patch
        # directly to stdout so it can be re-directed as-is
        print(f'Found bug #{bug} for {args.package}', file=sys.stderr)
        sys.stdout.buffer.write(patch)
    except Exception as e:
        print(f'Failed to get patch for {args.package}: {e}', file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    main()
