#!/bin/bash

. ./lib.sh

set -e
set -u
shopt -s inherit_errexit

sqlite3 "${SQLITE_DB}" \
    "select devpkg from packages inner join dumps on packages.rowid = dumps.id where dumps.status in ('dumped') order by devpkg asc"
