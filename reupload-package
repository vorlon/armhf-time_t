#!/usr/bin/python3

import pathlib
import shlex
import shutil
import subprocess
import sys


def run(cmd, **kw):
    msg = ["+"]
    for arg in cmd:
        arg = shlex.quote(str(arg))
        msg.append(arg)
    print(" ".join(msg))
    return subprocess.run(cmd, **kw)


cur_dir = pathlib.Path(".").resolve()
src_dir = pathlib.Path(sys.argv[0]).parent.resolve()

src_pkg = sys.argv[1]

work_dir = cur_dir.joinpath(src_pkg)

if not work_dir.exists():
    work_dir.mkdir()

try:
    src_pkg_dir = next(work_dir.glob(f"{src_pkg}-*/"))
except StopIteration:
    cp = run(
        ["chdist", "apt-get", "experimental", "source", "--only-source", src_pkg],
        cwd=work_dir,
    )
    if cp.returncode == 0:
        changelog = next(work_dir.glob(f"{src_pkg}-*/debian/changelog"))
        cp = run(
            ["dpkg-parsechangelog", "-SChanges", "-l", changelog],
            stdout=subprocess.PIPE,
            text=True,
        )
        if "Rename libraries for 64-bit time_t transition." in cp.stdout:
            print(f"{src_pkg} appears to have renaming changes in experimental already")
            sys.exit(0)

    cp = run(
        ["chdist", "apt-get", "unstable", "source", "--only-source", src_pkg],
        cwd=work_dir,
    )
    if cp.returncode != 0:
        print(f"could not download {src_pkg} from unstable!?")
        sys.exit(1)

    src_pkg_dir = next(work_dir.glob(f"{src_pkg}-*/"))
else:
    shutil.rmtree(src_pkg_dir)
    dsc = next(work_dir.glob(f"{src_pkg}_*.dsc"))
    run(["dpkg-source", "-x", dsc], cwd=work_dir, check=True)

patch_path = work_dir / f"nmu_{src_pkg}.debdiff"
if not patch_path.exists():
    try:
        with patch_path.open("w") as outf:
            cp = run(
                [src_dir / "grab-time-t-patch", sys.argv[1]],
                stdout=outf,
                text=True,
                check=True,
            )
    except subprocess.CalledProcessError:
        patch_path.unlink()
        raise

with patch_path.open() as inf:
    patch_text = inf.read()


filtered_patch_text = run(
    ["filterdiff", "-x", "*/debian/changelog"],
    input=patch_text,
    stdout=subprocess.PIPE,
    text=True,
    check=True,
).stdout

cp = run(
    ["patch", "-f", "-F0", "-p1"],
    input=filtered_patch_text,
    text=True,
    check=True,
    cwd=src_pkg_dir,
)


run(["dch", "-n", "Rename libraries for 64-bit time_t transition."], cwd=src_pkg_dir)
changelog = src_pkg_dir / "debian/changelog"
version = run(
    ["dpkg-parsechangelog", "-SVersion", "-l", changelog],
    stdout=subprocess.PIPE,
    text=True,
    check=True,
).stdout.strip()


def experimental_has_version(v):
    output = run(
        [
            "chdist",
            "grep-dctrl-sources",
            "experimental",
            "source",
            "-FPackage",
            "-w",
            src_pkg,
            "-a",
            "-FVersion",
            "-w",
            v,
        ],
        text=True,
        stdout=subprocess.PIPE,
    ).stdout
    return output.strip() != ""


V = 1
while experimental_has_version(f"{version}~exp{V}"):
    V += 1

run(["dch", "-b", "-v", f"{version}~exp{V}", ""], cwd=src_pkg_dir)
run(["dch", "-r", "-D", "experimental", ""], cwd=src_pkg_dir)

run(
    ["dpkg-buildpackage", "-S", "-nc"],
    check=True,
    cwd=src_pkg_dir,
)

sys.exit(0)

changes = next(work_dir.glob("*.changes"))

run(
    ["dput", "ftp-master", changes],
    check=True,
)
